$(function() {
  init_smooth_scroll();
});


function init_smooth_scroll() {  
  $('a[href*="#"]:not([href="#"])').click(function( event ) {
    event.preventDefault();
    var target = $( $(this).attr('href') );
    var target_offset = 0;
    if( target.length ) {         
      $('html, body').animate({scrollTop: target.offset().top - target_offset}, 1000);
    }
  });

  $('.scroll').click(function() {
    $("html, body").animate({ scrollTop: $('.np-next').offset().top - target_offset}, 1000);
  });

  $('.scroll-top').click(function() {
    $("html, body").animate({ scrollTop: $('#about').offset().top - target_offset}, 1000);
  });


  // When category choice buttons are clicked,
  // if active button is clicked, close category section.
  $('.category-choice input[type="radio"] + label[for]').on('click', function(e) {
    // console.log("$(this).prev('input'): ", $(this).prev('input'));
    if ($(this).prev('input').prop('checked')) {
      $(this).prev('input').prop('checked', false);
      return false;
    }
  });
};